<?php

function cartSummary(&$products) 
{
    $summary = ['totalCost' => 0, 'itemsCount' => 0];
    
    foreach ($products as $product) {
        $summary['totalCost'] += $product['price'];
        $summary['itemsCount'] += $product['quantity'];
    }
    
    return $summary;
}

$products = [
    ['name' => 'Телевизор', 'price' => 400, 'quantity' => 1],
    ['name' => 'Телефон', 'price' => 300, 'quantity' => 3],
    ['name' => 'Кроссовки', 'price' => 150, 'quantity' => 2],
];

echo '<pre>';

echo '//cartSummary function' . PHP_EOL;

print_r(cartSummary($products));

function solveQadraticEquation($a, $b, $c)
{
    $d = $b * $b - 4 * $a * $c;
    
    if ($d > 0)
    {
        $result = [
            'x1' => (-$b + sqrt($d)) / (2 * $a),
            'x2' => (-$b - sqrt($d)) / (2 * $a),
        ];
    }
    else if ($d == 0)
    {
        $result = -$b / (2 * $a);
    }
    else
    {
        $result = false;
    }
    
    return $result;
}

echo PHP_EOL . '//quadratic equation function' . PHP_EOL ;

var_dump(solveQadraticEquation(1, -1, -2));

function deleteNegatives($array)
{
    foreach ($array as $key => $value) {
        if ($value < 0) {
            unset($array[$key]);
        }
    }
    
    return $array;
}

function deleteNegativesByRef(&$array)
{
    foreach ($array as $key => $value) {
        if ($value < 0) {
            unset($array[$key]);
        }
    }
}

$digits = [2, -10, -2, 4, 5, 1, 6, 200, 1.6, 95];

echo PHP_EOL . '//deleteNegatives function' . PHP_EOL;

print_r(deleteNegatives($digits));

echo PHP_EOL . '//deleteNegativesByRef function' . PHP_EOL;

deleteNegativesByRef($digits);

print_r($digits);

echo '</pre>';
