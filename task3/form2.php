<!DOCTYPE html>
<?php
define(FORM_INPUT_NAME, 'name');
define(FORM_INPUT_GENDER, 'gender');
       
define(GENDER_MALE, 'М');
define(GENDER_FEMALE, 'Ж');
?>
<html>
<head></head>
<body>
    <form method="POST" action="">
        <!--    Setting properties this way seems pretty horrible, but ehh...-->
        <span>Имя</span>
        <input type="text" name="name" value="<?php echo $_REQUEST[FORM_INPUT_NAME]?>"/>
        <br>
        <span>Пол</span>
        <input type="radio" name="gender" value="М" 
            <?php echo ($_REQUEST[FORM_INPUT_GENDER] == GENDER_MALE ? 'checked' : '') ?>/>
        <span>М</span>
        <input type="radio" name="gender" value="Ж" 
            <?php echo ($_REQUEST[FORM_INPUT_GENDER] == GENDER_FEMALE ? 'checked' : '') ?>/>
        <span>Ж</span>
        <br>
        <input type="submit" value="Submit"/>
    </form>
    <div>
        <?php
        if (isset($_REQUEST[FORM_INPUT_NAME], $_REQUEST[FORM_INPUT_GENDER]))
        {
            echo 
                'Добро пожаловать, ' .
                ($_REQUEST[FORM_INPUT_GENDER] == GENDER_MALE ? 'мистер' : 'миссис') .
                ' ' .
                $_REQUEST[FORM_INPUT_NAME];
        }
        ?>
    </div>
</body>
</html> 