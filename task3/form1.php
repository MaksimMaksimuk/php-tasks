<!DOCTYPE html>
<?php
define(FORM_INPUT_VALUE, 'value');
?>
<html>
<head></head>
<body>
    <form method="POST" action="">
        <!--    Setting value this way seems pretty horrible, but ehh...-->
        <span>Value1:</span>
        <input type="text" name="value[]" value="<?php echo $_REQUEST[FORM_INPUT_VALUE][0]?>"/>
        <br>
        <span>Value2:</span>
        <input type="text" name="value[]" value="<?php echo $_REQUEST[FORM_INPUT_VALUE][1]?>"/>
        <br>
        <span>Value3:</span>
        <input type="text" name="value[]" value="<?php echo $_REQUEST[FORM_INPUT_VALUE][2]?>"/>
        <br>
        <span>Value4:</span>
        <input type="text" name="value[]" value="<?php echo $_REQUEST[FORM_INPUT_VALUE][3]?>"/>
        <br>
        <span>Value5:</span>
        <input type="text" name="value[]" value="<?php echo $_REQUEST[FORM_INPUT_VALUE][4]?>"/>
        <br>
        <span>Value6:</span>
        <input type="text" name="value[]" value="<?php echo $_REQUEST[FORM_INPUT_VALUE][5]?>"/>
        <br>
        <span>Value7:</span>
        <input type="text" name="value[]" value="<?php echo $_REQUEST[FORM_INPUT_VALUE][6]?>"/>
        <br>
        <input type="submit" value="Submit"/>
    </form>
    <div>
        <?php      
        if (isset($_REQUEST[FORM_INPUT_VALUE]))
        {
            foreach ($_REQUEST[FORM_INPUT_VALUE] as &$value) {
                $value = intval($value);
            }
            
            echo '<p>' . 'Maximum value: ' . max($_REQUEST[FORM_INPUT_VALUE]) . '</p>';
            echo '<p>' . 'Minimum value: ' . min($_REQUEST[FORM_INPUT_VALUE]) . '</p>';
            echo 
                '<p>' . 
                'Mean value: ' .
                array_sum($_REQUEST[FORM_INPUT_VALUE]) / count($_REQUEST[FORM_INPUT_VALUE]) .
                '</p>';
        }
        ?>
    </div>
</body>
</html>