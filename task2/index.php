﻿<html>
<head>
	<meta charset="UTF-8">
</head>
<body>
	<pre><?php
	
		//1: variable types
		
		$a = 152;
		$b = '152';
		$c = 'London';
		$d = array(152);
		$e = 15.2;
		$f = false;
		$g = true;
		
		echo '$a type: ' . gettype($a) . PHP_EOL;
		echo '$b type: ' . gettype($b) . PHP_EOL;
		echo '$c type: ' . gettype($c) . PHP_EOL;
		echo '$d type: ' . gettype($d) . PHP_EOL;
		echo '$e type: ' . gettype($e) . PHP_EOL;
		echo '$f type: ' . gettype($f) . PHP_EOL;
		echo '$g type: ' . gettype($g) . PHP_EOL;
	
		
		//2: strings
		
		$a = 5;
		$b = 10;
		
		echo PHP_EOL;
		
		echo "{$a} из {$b}ти студентов посетили лекцию" . PHP_EOL;
		echo $a . ' из ' . $b . 'ти студентов посетили лекцию' . PHP_EOL;
		
		$str1 = 'Доброе утро';
		$str2 = 'дамы';
		$str3 = 'и господа';
		
		echo PHP_EOL;
		
		echo 'str1: ' . $str1 . PHP_EOL;
		echo 'str2: ' . $str2 . PHP_EOL;
		echo 'str3: ' . $str3 . PHP_EOL;
		
		$concatResult .= $str1;
		$concatResult .= ', ';
		$concatResult .= $str2;
		$concatResult .= ' ';
		$concatResult .= $str3;
		
		echo PHP_EOL;
		
		echo $concatResult . PHP_EOL;
		
		//3: arrays
		
		$array1 = [1, 2, 3, 4, 5];
		$array2 = ['a', 'b', 'c', 'd', 'e'];
		
		$array1['element'] = 'whatever';
		
		unset($array2[0]);
		
		echo PHP_EOL;
		
		echo 'array1[2]: ' . $array1[2] . PHP_EOL;
		echo 'array2[2]: ' . $array2[2] . PHP_EOL;
		
		echo PHP_EOL;
		
		print_r($array1);
		print_r($array2);
		
		echo PHP_EOL;
		
		echo 'array1 size: ' . count($array1) . PHP_EOL;
		echo 'array2 size: ' . count($array2) . PHP_EOL;
		
		//conditional operator
		
		define('MIN', 10);
		define('MAX', 50);
		
		$x = 42;
		
		echo PHP_EOL;
		
		if ($x == MIN || $x == MAX)
		{
			echo '+-' . PHP_EOL;
		}
		else if ($x > MIN && $x < MAX)
		{
			echo '+' . PHP_EOL;
		}
		else
		{
			echo '-' . PHP_EOL;
		}
		
		$a = 1;
		$b = -1;
		$c = -2;
		
		$d = $b * $b - 4 * $a * $c;
		
		echo PHP_EOL;
		
		if ($d > 0)
		{
			echo 'x1 = ' . (-$b + sqrt($d)) / (2 * $a) . PHP_EOL;
			echo 'x2 = ' . (-$b - sqrt($d)) / (2 * $a) . PHP_EOL;
		}
		else if ($d == 0)
		{
			echo 'x = ' . -$b / (2 * $a) . PHP_EOL;
		}
		else
		{
			echo 'Нет корней' . PHP_EOL;
		}
		
		$a = 2;
		$b = 3;
		$c = 4;
		
		echo PHP_EOL;
		
		if ($a == $b || $a == $c || $b == $c)
		{
			echo 'Ошибка' . PHP_EOL;
		}
		else 
		{
			if (($a > $b) && ($a > $c))
			{
				echo 'Среднее число: ' . ($b > $c ? $b : $c) . PHP_EOL;
			}
			else if (($a < $b) && ($a < $c))
			{
				echo 'Среднее число: ' . ($b < $c ? $b : $c) . PHP_EOL;
			} 
			else
			{
				echo 'Среднее число: ' . $a . PHP_EOL;
			}
		}
		
		//cycles
		
		$sum = 0;
		
		for ($i = 1; $i <= 25; $i++)
		{
			$sum += $i;	
		}
		
		echo PHP_EOL;
		
		echo 'sum (for) = ' . $sum . PHP_EOL;
		
		$sum = 0;
		$i = 1;
		
		while ($i <= 25)
		{
			$sum += $i;
			$i++; 	
		}
		
		echo 'sum (while) = ' . $sum . PHP_EOL;
		
		$n = 200;
		
		echo PHP_EOL;
		
		for($i = 1; ($square = $i * $i) <= $n; $i++)
		{
			echo $i . ' ^ 2 = ' . $square . PHP_EOL;
		}  
	?></pre> 
	<ul>
		<?php
			$buttonList = 
			[
				'Кнопка 10',
				'Кнопка 9',
				'Кнопка 8',
				'Кнопка 7',
				'Кнопка 6',
				'Кнопка 5',
				'Кнопка 4',
				'Кнопка 3',
				'Кнопка 2',
				'Кнопка 1',
			];
			
			natsort($buttonList);
			
			foreach ($buttonList as $button)
			{
				echo '<li><a href="#">' . $button . '</a></li>';
			}
		?>
	</ul>
</body>
</html>
