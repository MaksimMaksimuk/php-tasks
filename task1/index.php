﻿<?php
//Maksim Maksimuk
/*
11.01.2017
*/
echo "Hello, World!";
?>
<html>
<head>
</head>
<body>
	<pre><?php
		$tvChannelName;
		$manufacturerAddress;
		$carColor;
		$waterTemperature;
		$phoneModel;
	
		$int1 = 3;
		$int2 = 5;
		$int3 = 8;
	
		echo "\nint1 = ", $int1,
			"\nint2 = ", $int2,
			"\nint3 = ", $int3;
		
		$intSum = $int1 + $int2 + $int3;

		echo "\n\nintSum = ", $intSum;

		$result = 2 + 6 + 2 / 5 - 1;
		
		echo "\n\nresult(2+6+2/5-1) = ", $result;

		$a;
		$b;
		$c;
		$d;

		$a = 1;
		$b = 2;
		
		echo "\n\na = ", $a,
			"\nb = ", $b;
		
		$c = $a;
		$d = &$b;

		echo "\n\nc = ", $c,
			"\nd = ", $d;
		
		$a = 3;
		$b = 4;

		echo "\n\na = ", $a,
			"\nb = ", $b,
			"\nc = ", $c,
			"\nd = ", $d;

		define("INT_41", 41);
		define("INT_33", 33);

		echo "\n\nconstSum = ", INT_41 + INT_33;
	?><pre>	
</body>
</html>